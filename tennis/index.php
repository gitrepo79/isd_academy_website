<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Tennis Page";
    $keywords = "";
    $desc = "";
    $pageclass = "tennispg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner" style="background-color: #01b1ae">
	<img src="/assets-web/images/tennis-banner.jpg" alt="" class="m-auto">
</section>

<!-- Overview -->
<section class="sec-padding" style="padding-bottom: 0">
	<div class="container">
		<h2 class="maintitle fc-primary tt-uppercase mbpx-30">
			Tennis Overview
		</h2>

		<p class="maindesc fc-primary">
			ISD Academy’s tennis program sets the standard by which all tennis academies around the region are measured and provides student-athletes with the resources they need to perform at the highest level. Student-athletes progress technically, tactically, physically, and mentally while also seeing personal growth through character development and leadership skills. <br><br>

			ISD continues to be a premier destination to the top ATP/WTA players in the Middle East, allowing student-athletes to train within an incomparable atmosphere. Players are challenged on a daily basis to reach their full potential and become champions on and off the court. For this reason, ISD Academy tennis players have a direct pathway to professional tennis careers or athletic scholarship opportunities to US, UK, or Spanish colleges and universities.
		</p>
	</div>
	
	<picture class="mtpx-30 d-block">
		<img src="/assets-web/images/tennis-overview.jpg" alt="">
	</picture>
</section>

<!-- Technology Facilites -->
<section class="bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Technology & Facilities
				</h2>

				<p class="maindesc">
					• Four professionally-maintained, hardcourt, outdoor tennis courts <br>
					• Indoor turf field for speed and agility work on days with high temperatures <br /><br />
                    <strong style="color: #00b1ae">Eupepsia Performance Lab: </strong> state-of-the-art strength and conditioning facility focused on optimizing athletic performance through baseline testing and periodic measurement followed by sport-specific, personalized training program to attack deficiencies and maximize strengths <br /><br />
                    
                    <strong style="color: #00b1ae">Eupepsia Recovery:</strong> whole-body cryotherapy, oxygen & hydrogen therapy, compression remedy, magneto therapy, and infrared sauna <br /><br />
                    
                    <strong style="color: #00b1ae">Eupepsia Medical Clinic:</strong> sports physiotherapy, recovery, and conditioning, wellness therapies, diet & nutrition, and ayurvedic medicine 
				</p>

			</div>

		</div>
	</div>
</section>


<!-- Program Structure -->
<section class="sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle fc-primary tt-uppercase lh-medium mbpx-30">
					Tennis Program Structure
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="box --football-program-structure">
					<h5 class="title mbpx-20" style="color: #7a56b8">First Term (16 weeks)</h5>

					<p class="fc-primary">
						• Preseason and Measurements <br>
						  (4-6 weeks) <br>
						• General strength & conditioning <br>
						• Fundamentals correcting & teaching <br>
						• Technical & tactical abilities <br>
						• Acceleration & explosiveness training <br>
						• Baseline measurements &   
						  assessments <br>
						• Competitive Season (8-10 weeks) <br>
						• Maintain conditioning level
						  (EPL & track) <br>
						• Injury prevention (EPL) <br>
						• Improving serving technique <br>
						• Tactical skills sessions (match 
						  understanding & decision making) <br>
						• Competition preparation (tactics) <br>
						• Mental training & preparation <br>
						• Video analysis sessions <br>
						• Competition - tournaments, leagues, 
						  and matches  <br> <br>

						BREAK WITH 2 WEEKS OF REST <br>
						Individualized strength and conditioning plans to keep in shape and prevent injury

					</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box --football-program-structure">
					<h5 class="title mbpx-20" style="color: #7a56b8">Second term (12 weeks)</h5>

					<p class="fc-primary">
						• Preseason and Measurements <br>
						  (2 weeks) <br>
						• General strength & conditioning <br>
						• Fundamentals correcting & teaching <br>
						• Technical & tactical abilities <br>
						• Acceleration & explosiveness training <br>
						• Measurements & assessments <br>
						• Competitive Season (10 weeks) <br>
						• Maintain conditioning level
						  (EPL & track) <br>
						• Injury prevention (EPL) <br>
						• Improving serving technique <br>
						• Tactical skills sessions (game 
						  understanding & decision making) <br>
						• Competition preparation (tactics) <br>
						• Mental training & preparation <br>
						• Video analysis sessions <br>
						• Competition - tournaments, leagues, 
						  and matches <br> <br>

						BREAK WITH 2 WEEKS OF REST (spring break) <br>
						Individualized strength and conditioning plans to keep in shape and prevent injury
					</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box --football-program-structure">
					<h5 class="title mbpx-20" style="color: #7a56b8">Third term (12 weeks)</h5>

					<p class="fc-primary">
						• General strength & conditioning <br>
						• Fundamentals correcting & teaching <br>
						• Technical & tactical abilities <br>
						• Acceleration & running mechanics 
						  training <br>
						• Measurements & assessments <br>
						• Maintain conditioning level
						  (EPL & track) <br>
						• Injury prevention (EPL) <br>
						• Improving serving technique <br>
						• Tactical skills sessions (game 
						  understanding & decision making) <br>
						• Competition preparation (tactics) <br>
						• Mental training & preparation <br>
						• Video analysis sessions <br>
						• Competition - tournaments, matches, 
						  and international competition*
					</p>
				</div>
			</div>
		</div>


	</div>
</section>


<!-- Development Process -->
<section class="bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					development process
				</h2>
				
				<picture class="mtpx-30 d-block">
            		<img src="/assets-web/images/tennis-development-process.jpg" alt="">
            	</picture>

				<p class="maindesc mtpx-30">
					ISD Academy tennis provides a professional atmosphere for young student-athletes to improve their skills under the watchful, meticulous eyes of coaches that have proven themselves to be some of the best teachers in the Middle East, with five past players currently competing with colleges and universities in the United States. The coaching staff has the knowledge and connections to bring out the highest performance level within each athlete and successfully place them in collegiate or professional tennis programs. 
				</p>

			</div>

		</div>
	</div>
</section>

<!-- LaLiga HPC -->
<section class="sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle fc-primary tt-uppercase lh-medium mbpx-30">
					International Competition
				</h2>
				
				<picture class="mtpx-30 d-block">
            		<img src="/assets-web/images/tennis-international-competition.jpg" alt="">
            	</picture>

				<p class="maindesc fc-primary mtpx-30">
					At the end of the competitive season in Dubai, ISD Tennis players and coaches will take a one-month trip internationally to test their skills in competition against the top players in Europe. This is an incredible opportunity to compete in front of coaches from colleges and universities in Europe and professional scouts. 
				</p>

			</div>

		</div>
	</div>
</section>

<?php include ($path.'/inc/footer.php'); ?>