// JavaScript Document
$(function(){

    //*****************************
    // Reset Href
    //*****************************
    $('[href="#"]').attr("href","javascript:;");

    //*****************************
    // Smooth Scroll
    //*****************************
    function goToScroll(e){
        $('html, body').animate({
            scrollTop: $("."+e).offset().top
        }, 1000);
    }

    //*****************************
    // Lazy Load
    //*****************************
    $(window).scroll(function(){
        lazzyload();
    });
    
    //*****************************
    // Mobile Navigation
    //*****************************
    $('.mobile-nav-btn').click(function() {
        $('.mobile-nav-btn, .mobile-nav').toggleClass('nav-active');
        //$('.overlay-bg').fadeIn();
    });


    $('.mobile-navigation .close-btn').click(function(){
        $('.mobile-nav-btn, .mobile-nav').removeClass('nav-active');
    //     $(this).fadeOut();
    });

    $('.mobile-navigation .main-nav a').click(function(){
        $('.mobile-nav-btn, .mobile-nav').removeClass('nav-active');
    });
    
    // $('.overlay-bg').click(function(){
    //     $('.mobile-nav-btn, .mobile-nav, .app-container').removeClass('active');
    //     $(this).fadeOut();
    // });


    //*****************************
    // Scroll Funtion
    //*****************************

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        // if (scroll >= 1) {
        //     $('.mobile-contact').addClass('active');
        // } else {
        //     $('.mobile-contact').removeClass('active');
        // }

        if (scroll >= 1) {
            $('.primary-header').addClass('scroll');
        } else {
            $('.primary-header').removeClass('scroll');
        }
    });


    //*****************************
    // Slick Slider
    //*****************************
    $('.hero-slider').slick({
        arrows: true,
        dots: false,
        infinite: true,
        autoplay:true,
        speed: 600,
    });


	var respsliders = {
      1: {slider : '.slider1'},
      2: {slider : '.slider2'}
    };

    $.each(respsliders, function() {

        $(this.slider).slick({

            arrows: true,
            dots: false,
            infinite: true,
            autoplay:true,
            speed: 300,
            responsive: [
                {
                    breakpoint: 2000,
                    settings: "unslick"
                },
                {
                    breakpoint: 768,
                    settings: {
                        unslick: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });

    //*****************************
    // File Browser
    //*****************************
    xfilebrowse('.file-upload');
    function xfilebrowse(tgt){  
        $(tgt+' input[type=file]').each(function() {
            $(this).wrap('<div class="upldwrap" />');
            $(this).parent().append('<span class="browse">Choose Files</span> <label class="filelabel">Upload your files</label>');
            $(this).css('opacity', 0);
            $(this).on('change', function() {
                var txt = $(this).val();
                if(txt !== ''){
                    txt = txt.replace(/\\/g, '/').replace(/.*\//, '');
                    $(this).siblings('.filelabel').html(txt);
                }else{
                    $(this).siblings('.filelabel').html('No File Selected');
                }                
            })
        });
    }
    

    //*****************************
    // Accordian
    //*****************************
    $('.accordian').click(function() {
        $(this).parent().children('.accordian-inner').slideToggle();
    });

    //*****************************
    // Fancybox
    //*****************************
    $('[data-fancybox="gallery"]').fancybox({
        // Options will go here
    });

    //*****************************
    // Form Animation
    //*****************************
    $('.form-field').on('focus blur',function(i){
        $(this).parents('.control-group').toggleClass('focused','focus'===i.type||this.value.length>0)
    }).trigger('blur');

    //*****************************
    // Tabbing
    //*****************************
    $('[data-targetit]').on('click',function () {
        $(this).siblings().removeClass('current');
        $(this).addClass('current');
        var target = $(this).data('targetit');
        $('.'+target).siblings('[class^="tabs"]').removeClass('current');
        $('.'+target).addClass('current');
    });

    //*****************************
    // Copyright Year
    //*****************************
    now=new Date;thecopyrightYear=now.getYear();if(thecopyrightYear<1900)thecopyrightYear=thecopyrightYear+1900;$("#cur-year").html(thecopyrightYear);

    //*****************************
    // Set Map
    //*****************************
    $("address.setmap").each(function(){
        var embed ="<iframe frameborder='0' scrolling='no' marginheight='0' height='100%' width='100%' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+ encodeURIComponent( $(this).text() ) +"&amp;output=embed'></iframe>";
        $(this).html(embed);
    });
    
    
    //*****************************
    // Sort Boxes 
    //*****************************
    var $sorted_items_large, $sorted_items_small,
    getSorted = function(selector, attrName) {
          return $(
            $(selector).toArray().sort(function(a, b){
                var aVal = parseInt(a.getAttribute(attrName)),
                    bVal = parseInt(b.getAttribute(attrName));
                return aVal - bVal;
            })
          );
    };
    
    // Sort Large Boxes
    if($('.col-md-6.large-box')){
        $sorted_items_large = getSorted('.col-md-6.large-box', 'data-order');
        $('#sorted-largel-box').html( $sorted_items_large );
    }
    
    // Sort Small Boxes
    if($('.col-md-3.small-box')){
        $sorted_items_small = getSorted('.col-md-3.small-box', 'data-order');
        $('#sorted-small-box').html( $sorted_items_small );
    }
    

});