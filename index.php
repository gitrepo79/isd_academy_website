<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Home Page";
    $keywords = "";
    $desc = "";
    $pageclass = "homepg";
    $section_layer = "Home Page";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner">
	<img src="/assets-web/images/homepage-banner.jpg" alt="">
</section>

<section class="about-section bg-primary sec-padding">
	<div class="container">
		<h2 class="maintitle tt-uppercase mbpx-20">
			isd international academy
		</h2>

		<p class="maindesc">
			provides student-athletes from across the world an elite education while receiving world-class sports coaching in facilities honed to deliver the perfect developmental environment. The Academy is a centralized, walkable campus community in the heart of the fastest-growing city in the world: Dubai. <br><br>

			At ISD International Academy, we provide athletes with a proven pathway to earn an opportunity in professional sports, or receive a scholarship to a college or university in the United States or United Kingdom. By furthering their education through our IB, A-level, and U.S. curriculum options, our students will open avenues to high-level academic opportunities with sporting excellence at its heart. <br><br>

			We utilize our own established academies and development programs to offer year-round training regimens in football, tennis, rugby, athletics, basketball, cricket, and padel within Dubai Sports City.
		</p>
	</div>
</section>

<section class="programs-section bg-secondary sec-padding">
	<div class="container">
		<h2 class="maintitle tt-uppercase text-center mbpx-30">
			AN ELITE PROGRAM FOR EVERYONE
		</h2>

		<div class="row">
			<div class="col-lg-4">
				<div class="box --program-box">
					<h4 class="fc-primary tt-uppercase mbpx-10 text-center">
						sports development
					</h4>
					<picture>
						<img src="/assets-web/images/sports-development.jpg" alt="" class="m-auto">
					</picture>
					<p class="maindesc mtpx-20">
						ISD International Academy offers programs revolving around 7 different sports: football, tennis, rugby, athletics, basketball, cricket, and padel. Each program is delivered by teams of globally certified coaches, with bespoke development pathways to take each student athlete to their desired goal. <a href="#" class="link-btn">Find out more</a>
					</p>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --program-box">
					<h4 class="fc-primary tt-uppercase mbpx-10 text-center">
						EDUCATION
					</h4>
					<picture>
						<img src="/assets-web/images/education.jpg" alt="" class="m-auto">
					</picture>
					<p class="maindesc mtpx-20">
						Providing a stellar education is of the highest importance at ISD International Academy, which is why we have partnered with the leading educational institutes throughout Dubai to deliver IB, AP, and A-level curriculums ensuring every possible developmental avenue is delivered to <a href="#" class="link-btn">Find out more</a>
					</p>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="box --program-box">
					<h4 class="fc-primary tt-uppercase mbpx-10 text-center">
						FACILITIES & TECHNOLOGY
					</h4>
					<picture>
						<img src="/assets-web/images/facilities.jpg" alt="" class="m-auto">
					</picture>
					<p class="maindesc mtpx-20">
						Home to world-class multi-sports academies, playing venues, entertainment parks and much more, Inspiratus Sports District has transformed Dubai’s Sporting Scene. Located in the heart of Dubai, it is Dubai’s premier destination for football, athletics, rugby, tennis, padel, and much more <a href="/facilities" class="link-btn">Find out more</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="partners-section sec-padding">
	<div class="container">
		<h2 class="maintitle tt-uppercase text-center fc-primary">
			our partners
		</h2>

		<div class="partner-slider">
			<div class="item">
				<img src="/assets-web/images/logos/laliga-logo.png" alt="">
			</div>

			<div class="item">
				<img src="/assets-web/images/logos/eupepsia-logo.png" alt="">
			</div>

			<div class="item">
				<img src="/assets-web/images/logos/footlab.png" alt="">
			</div>

			<div class="item">
				<img src="/assets-web/images/logos/dwight-logo.png" alt="">
			</div>

			<div class="item">
				<img src="/assets-web/images/logos/laliga-logo.png" alt="">
			</div>
		</div>
	</div>
</section>

<section class="why-dubai sec-padding bg-primary">
	<div class="container">
		<h2 class="maintitle tt-uppercase text-center fc-white mbpx-20">
			why dubai
		</h2>

		<picture>
			<img src="/assets-web/images/why-dubai.jpg" alt="">
		</picture>

		<p class="maindesc mtpx-30">
			Hailed as one of the most progressive and safe cities in the world, Dubai is among the most attractive places to live, work and develop. Studying, training, and living in Dubai opens students to a world of possibilities, providing them with a strong and empowering foundation for their lifelong tambitions. <br><br>

			Dubai’s phenomenal growth, unparalleled services, buzzing cultural and cosmopolitan scene, beautiful beaches, and year-round sunshine have made it a top tourist, business, and sports destination worldwide. Offering students a wide range of athletics, cultural, and tourist activities in one of the world’s most diverse cities, Dubai helps students gain new perspectives, while making friends and connections from across the globe. While developing their academic skills, it will also nurture their personal growth with valuable lifelong skills they will carry with them forever. <br><br>

			Students will also have the unique opportunity to cross paths with the world’s top athletes, who frequently visit Inspiratus Sports District for training and competitions in global events. ISD has become their destination of choice, recognized for its sporting excellence that meets each athlete’s highest standards.
		</p>
	</div>
</section>

<?php include ($path.'/inc/footer.php'); ?>