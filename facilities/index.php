<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Facilities Page";
    $keywords = "";
    $desc = "";
    $pageclass = "facilitiespg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner" style="background-color: #01b1ae">
	<img src="/assets-web/images/facilities-banner.jpg" alt="" style="object-fit: cover" class="m-auto">
</section>

<section class="about-section sec-padding">
	<div class="container">
		<h2 class="maintitle fc-primary mbpx-20">
			SPORTS, PERFORMANCE & NUTRITION
		</h2>

		<p class="maindesc fc-primary">
			Inspiratus Sports District along with its partners located on the same campus provide 10 different facilities, each one hand-picked and developed with the sole purpose of creating the perfect environment for sports development. Click on each of the below icons to expand upon each facility. 
		</p>

		<hr class="h-4 mtpx-40 mbpx-40 bg-secondary">

		<div class="grid-block --type-five-blocks --mobile-quarter-grid">
			<div class="item mbpx-20">
				<picture>
					<a href="#football">
					    <img src="/assets-web/images/logos/isd-football.png" alt="">
					</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#athletics">
    					<img src="/assets-web/images/logos/isd-athletics.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#tennis">
    					<img src="/assets-web/images/logos/isd-tennis.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#padel">
    					<img src="/assets-web/images/logos/isd-padel.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#basketball">
    					<img src="/assets-web/images/logos/isd-basketball.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#footlab">
    					<img src="/assets-web/images/logos/footlab-logo.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#icc">
    					<img src="/assets-web/images/logos/icc-logo.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#eupepsia-medical">
    					<img src="/assets-web/images/logos/eupepsia-medical.png" alt="">
    				</a>
				</picture>
			</div>

			<div class="item mbpx-20">
				<picture>
				    <a href="#eupepsia-performance">
    					<img src="/assets-web/images/logos/eupepsia-performance.png" alt="">
    				</a>
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Football -->
<section id="football" class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6">
				<h2 class="maintitle tt-uppercase mbpx-20">
					football
				</h2>

				<p class="maindesc mbpx-20">
					Inspiratus Sports District is home to 5 beautiful, FIFA-standard and UAEFA-approved football pitches. This is comprised of 3 outdoor natural grass, 1 outdoor artificial turf, and 1 indoor full-size 3G turf pitch. Each of these fields is maintained at the highest of international standards and are utilized by professional teams from Europe and Asia, various Dubai football leagues, and ISD’s LaLiga Football Academy. 
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/football-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Footlab -->
<section id="footlab" class="facilities-box left-bg sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6 order-md-last">
				<h2 class="maintitle tt-uppercase fc-primary mbpx-20">
					footlab
				</h2>

				<p class="maindesc fc-primary mbpx-20">
					Launched in June of 2021 by football legend Rui Costa, Footlab Dubai has become a lively destination for athletes of all levels of skill. Players are led through four individual stations that measure speed, technique of dribbling, kick power, and touch accuracy, followed by three group stations - Challenge, Footvolley, and Street Soccer - which provide an exciting method of close-quarters training that many professional clubs throughout Europe and Asia have adopted into their own regimens.
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/footlab-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Athletics -->
<section id="athletics" class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6">
				<h2 class="maintitle tt-uppercase mbpx-20">
					Athletics
				</h2>

				<p class="maindesc mbpx-20">
					The Stadium at Inspiratus Sports District seats up to 2,500 spectators, and is home to a well-maintained, Olympic-standard 9-lane running track. Within the stadium are two full-size football and rugby pitches that are also utilized for javelin, shotput, and discus. Also on site are top-notch facilities for high jump, long jump, and triple jump, with pole-vault being added soon.
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/athletics-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Tennis -->
<section id="tennis" class="facilities-box left-bg sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6 order-md-last">
				<h2 class="maintitle tt-uppercase fc-primary mbpx-20">
					tennis
				</h2>

				<p class="maindesc fc-primary mbpx-20">
					ISD Tennis’ four brand new, state-of-the-art flood lit outdoor tennis courts surrounded by their peripheral facilities including the Olympic running track and Eupepsia Performance Lab, provide the best setting for student athletes to train, develop and mature into world-class professionals.
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/tennis-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Padel -->
<section id="padel" class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6">
				<h2 class="maintitle tt-uppercase mbpx-20">
					Padel
				</h2>

				<p class="maindesc mbpx-20">
					Opening in May of 2022, ISD Padel at Dubai Sports City is the new home of Padel in the UAE. With 6 full size indoor courts, one singles and 2 full size and outdoor courts, racket-specific indoor warm-up areas, ISD Padel is one of the largest most advanced purpose-built padel complexes in the region and will play host to our academy players and the highest level of professional tournaments for years to come.
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/padel-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Basketball -->
<section id="basketball" class="facilities-box left-bg sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6 order-md-last">
				<h2 class="maintitle tt-uppercase fc-primary mbpx-20">
					basketball
				</h2>

				<p class="maindesc fc-primary mbpx-20">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. 
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/basketball-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Rugby -->
<section id="rugby" class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6">
				<h2 class="maintitle tt-uppercase mbpx-20">
					Rugby
				</h2>

				<p class="maindesc mbpx-20">
					ISD features a state-of-the-art rugby facility boasting two World Rugby standard playing surfaces, which hosted the 2017 & 2018 UAE Rugby Finals days as well as UAE international games. It is a preferred destination for top international 7s teams in the lead up to the Dubai 7s every year. We have a dedicated clubhouse, which is always lively on match days. It’s the top destination in Dubai to enjoy local rugby and socialize with the community.
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/rugby-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Cricket -->
<section id="icc" class="facilities-box left-bg sec-padding">
	<div class="container">	
		<div class="row align-items-center">
			<div class="col-lg-4 col-md-6 order-md-last">
				<h2 class="maintitle tt-uppercase fc-primary mbpx-20">
					Cricket
				</h2>

				<p class="maindesc fc-primary mbpx-20">
					ICC Academy will be the host venue for ISD Academy cricket players. Located directly adjacent to Inspiratus Sports District, ICC Academy is managed and administered by the International Cricket Council, the world governing body of cricket. Combining ICC’s world-class coaches and facilities and ISD Academy’s new-wave developmental tools, ISD Cricket is the destination of choice for cricket players around the world!
				</p>
			</div>

			<div class="col-lg-8 col-md-6">
				<picture>
					<img src="/assets-web/images/cricket-facility.jpg" alt="">
				</picture>
			</div>
		</div>
	</div>
</section>

<!-- Eupepsia Performance -->
<section id="eupepsia-performance" class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					eupepsia <br> performance lab
				</h2>

				<picture class="mbpx-30 d-block">
					<img src="/assets-web/images/eupepsia-performance-facility.jpg" alt="">
				</picture>

				<p class="h4">
					The Eupepsia Performance Lab is a state-of-the-art strength, conditioning, and recovery facility focused on optimizing athletic performance. 
				</p>
			</div>

			<div class="col-lg-4">
				<p class="maindesc mtpx-30">
					<strong style="color: #187de0">MEASURE:</strong> Through baseline testing and periodic measurement, athletes are able to measure their progress and performance over time. Data is collected using cutting edge sports science technologies which allows the talented team of physiotherapists and bioengineers to craft individual strength & conditioning programs.
				</p>
			</div>

			<div class="col-lg-4">
				<p class="maindesc mtpx-30">
					<strong style="color: #187de0">PERFORM:</strong> Eupepsia’s performance programs evolve around the latest advances and available research in Sports Science applying technology and equipment used by elite level athletes, international teams and top Olympic training facilities.
				</p>
			</div>

			<div class="col-lg-4">
				<p class="maindesc mtpx-30">
					<strong style="color: #187de0">RECOVER:</strong> Eupepsia Performance Lab brings the best recovery technologies from elite athletes and makes them available to budding student athletes at ISD International Academy. Advanced technologies are used for daily recovery.
				</p>
			</div>

		</div>
	</div>
</section>

<!-- Eupepsia Clinig -->
<section id="eupepsia-medical" class="facilities-box left-bg sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle fc-primary tt-uppercase lh-medium mbpx-30 text-md-right">
					eupepsia <br> medical clinic
				</h2>

				<picture class="mbpx-30 d-block">
					<img src="/assets-web/images/eupepsia-medical-facility.jpg" alt="">
				</picture>

				<p class="maindesc fc-primary">
					Eupepsia Medical Clinic offers a unique approach to sports and wellness management through integrating natural and progressive sports and wellness therapies to address various imbalances and improve fitness and performance with preventative, strengthening, and recovery systems for athletes including athlete assessments and nutrition programs. <br><br>

					Drawing on the science of Ayurveda, dietetics, nutrition and physiotherapy, combined with the most advanced health screening and medical analysis, Eupepsia goes beyond treating symptoms to finding and treating the root cause providing effective solutions with lasting impact.
				</p>
			</div>

		</div>
	</div>
</section>

<?php include ($path.'/inc/footer.php'); ?>