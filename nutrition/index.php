<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Nutrition Page";
    $keywords = "";
    $desc = "";
    $pageclass = "nutritionpg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner" style="background-color: #01b1ae">
	<img src="/assets-web/images/nutrition-banner.jpg" alt="" class="m-auto">
</section>

<section class="sec-padding">
	<div class="container">
		<h2 class="maintitle fc-primary tt-uppercase mbpx-30">
			Theo’s Point
		</h2>

		<p class="maindesc fc-primary">
			Student-athletes of ISD Academy receive three meals, with morning and afternoon snacks, seven days a week from Theo’s Point, delivered each morning to their apartment door! <br><br>

			Theo's Point is an outcome-based meal plan provider that has been founded on a philosophy of creating long term health and well-being benefits through customizing meal plans to each person's body type. Theo’s Point seeks to empower people, with the assistance of Eupepsia Wellness Center and their sports nutrition experts, to awaken their inner intelligence and guide them, through personalized experience, to sustain a long-term healthy lifestyle. The core of the offering is balanced meals with only natural ingredients: no preservatives, no cans, no hormones, and treated organically, to act as a natural detox to the whole system.
		</p>
	</div>
</section>


<section class="bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Sports Nutrition
				</h2>

				<p class="maindesc">
					Each athlete at ISD Academy will sit down with a sports nutritionist from Theo’s Point in their first week on campus to design a meal plan that meets their specific body and performance goals. With options such as “fitness,” ‘weight management,’ ‘healthy best,’ ‘vegan,’ and ‘managing intolerances,’ there is a meal plan that fits each student-athlete’s nutritional requirements. Plans can also be further personalized to meet specific dietary restrictions or preferences. <br><br>

					Once the meal plan is underway, student-athletes will have a meeting scheduled with our Theo’s Point nutritionists every two weeks to check in on their progression of body and performance aspirations. In this one hour, bi-weekly meeting, the nutritionist will perform a series of tests that measure parameters such as weight, body fat mass and percentage, and total body water, proteins, and minerals. He or she will then analyze the results for our athletes to give them a more complete understanding of what they have been improving upon, and where they will require more focus. This analysis will allow the Theo’s Point team to further customize the athlete’s meal plan to achieve maximum results!
				</p>

				<picture class="mtpx-30 d-block">
					<img src="/assets-web/images/sports-nutrition.jpg" alt="">
				</picture>

			</div>

		</div>
	</div>
</section>

<?php include ($path.'/inc/footer.php'); ?>