<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Education Page";
    $keywords = "";
    $desc = "";
    $pageclass = "educationpg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner" style="background-color: #01b1ae">
	<img src="/assets-web/images/education-banner.jpg" alt="" style="object-fit: cover" class="m-auto">
</section>

<section class="about-section sec-padding">
	<div class="container">
		<h2 class="maintitle fc-primary tt-uppercase mbpx-20">
			Dwight School
		</h2>

		<p class="maindesc fc-primary">
			SD Academy has partnered with Dwight Schools to provide the highest level of AP and IB curriculums available. With an online program recently ranked the #2 in the United States by Newsweek magazine, Dwight has a rich, 150-year heritage and worldwide network with proven success in creating hybrid programs for students aged 13-18 to maximize their academic achievements. <br><br>

			Small class size, discussion, and collaboration are prioritized in both their in-person and online curriculums. Along with world-class teachers, Dwight also provides dedicated college counseling and access to the best universities and colleges around the world.  Countless alumni attend and have attended Ivy League schools and other highly-acclaimed U.S. and U.K. colleges and universities. Dwight is dedicated to igniting the spark of genius in every child. Through the kindling of their students’ interests, Dwight develops inquisitive, knowledgeable, self-aware, and ethical citizens who will build a better world.
		</p>
	</div>
</section>

<!-- Classroom Learning -->
<section class="facilities-box bg-secondary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Dwight School Dubai <br>Classroom Learning
				</h2>

				<picture class="mbpx-30 d-block">
					<img src="/assets-web/images/wright-classroom.jpg" alt="">
				</picture>

				<p class="maindesc">
					With their brand-new facility located just a few minutes walk from Inspiratus Sports District, ISD-registered Dwight students will be able to attend in-person classes on their beautiful campus for an additional cost. Dwight is committed to providing high quality holistic education in a supportive, caring, and collaborative learning environment. Classroom students sacrifice the flexibility of their schedule in order to gain a wholesome education experience with in-person labs and more face-to-face interactions with peers. These students will learn more advanced time-management skills while balancing elite education and athletics. At Dwight School Dubai, students have a great opportunity to develop strong, meaningful relationships with their teachers, more than with an online curriculum. 
				</p>
			</div>

		</div>
	</div>
</section>

<!-- Distance Learning -->
<section class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Dwight Global Online School <br> Distance Learning
				</h2>

				<picture class="mbpx-30 d-block">
					<img src="/assets-web/images/wright-distance.jpg" alt="">
				</picture>

				<p class="maindesc">
					With Dwight Global Online, students experience the personalization of a U.S. private school with the increased flexibility to pursue their passion for sport. Dwight Global’s faculty are experts in their fields and understand the commitment our students have to both their academics and their athletics. Dwight’s scheduling allows students to meet for their live online classes and arrive on time for sports training and matches. Students meet for class in live online seminars with daily attendance taken to ensure participation, and maximize their deep learning through small class sizes, discussion, and collaboration.
				</p>
			</div>

		</div>
	</div>
</section>

<!--  -->
<section class="bg-secondary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-4">
				<div class="box --education-boxes">
					<picture>
						<img src="/assets-web/images/logos/courses-icon.png" alt="" class="m-auto mbpx-40">
					</picture>
					<article>
						<h4 class="fc-primary tt-uppercase mbpx-20 lh-medium">
							courses
						</h4>

						<p class="maindesc">
							Dwight Global offers over 100 college preparatory, Honors, Advanced Placement (AP), and International Baccalaureate (IB) classes for grades 7 to 12. Courses are American-style and NCAA approved. All core classes not designated as AP or IB may be taken at the Honors level, with permission. Through this vast offering of subjects and class, we know that there is a path for each student to design a pathway to maximize their goals and achievements.
						</p>
					</article>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box --education-boxes">
					<picture>
						<img src="/assets-web/images/logos/student-support-icon.png" alt="" class="m-auto mbpx-40">
					</picture>
					<article>
						<h4 class="fc-primary tt-uppercase mbpx-20 lh-medium">
							Student Support
						</h4>

						<p class="maindesc">
							Each ISD-Dwight Global athlete will be assigned a Dean, who advocates for the student to have a rewarding online academic experience. Whether a student has an academic concern, wants to find a new extracurricular opportunity, or hopes to add/drop a class, the Dean serves as the first point of contact for ISD students, parents, and coaches. Extensive additional academic support is available to ISD Academy students on an as-needed basis. Support includes supervised study halls, 1-on-1 academic tutorials with Dwight teachers, and organizational coaches. The Quest program also provides more extensive support to students with documented learning differences(for a fee).
						</p>
					</article>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box --education-boxes">
					<picture>
						<img src="/assets-web/images/logos/development-program-icon.png" alt="" class="m-auto mbpx-40">
					</picture>
					<article>
						<h4 class="fc-primary tt-uppercase mbpx-20 lh-medium">
							Summer and English Language Development Programs
						</h4>

						<p class="maindesc">
							Full-credit summer courses are offered, giving ISD student-athletes flexibility to spread their academic load over the course of a full year. A final list of summer of 2022 courses will be released on April 1, 2022. Summer courses run from mid-June to late August. Dwight will also offer optional English learning support to our international student-athletes, which can be taken during the school year or over the summer.
						</p>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Colleges -->
<section class="bg-primary sec-padding">
	<div class="container">	
		<h2 class="maintitle --medium tt-uppercase mbpx-60">
			DWIGHT SCHOOL PROVIDES ACCESS TO OVER 100 COLLEGE PREPARATORY CLASSES
		</h2>

		<h2 class="fc-secondary tt-uppercase mbpx-10">
			AP Curriculum
		</h2>

		<p class="maindesc">
			Dwight Global offers over 100 college preparatory Honors, Advanced Placement (AP), and International Baccalaureate (IB) classes for grades 7 to 12. Courses are American-style and NCAA approved. All core classes not designated as AP may be taken at the Honors level, with permission. The vast selection of Honors and AP courses are listed below:
		</p>

		<div class="row mtpx-40 college-courses">
			<div class="col-lg-3 col-md-4 col-sm-6">
				<h4 class="title fc-secondary tt-uppercase">
					Math
				</h4>

				<p class="maindesc">
					• Pre-algebra <br>
					• Algebra <br>
					• Geometry <br>
					• Algebra II & Trig <br>
					• Precalculus <br>
					• Calculus <br>
					• AP Statistics <br>
					• AP Calculus AB <br>
					• AP Calculus BC <br>
					• Multivariable Calculus
				</p>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<h4 class="title fc-secondary tt-uppercase">
					Math
				</h4>

				<p class="maindesc">
					• Earth & Environmental (MS) <br>
					• Physical Science (MS) <br>
					• Biology <br>
					• Chemistry <br>
					• Physics <br>
					• Anatomy & Physiology <br>
					• Earth & Environmental Sci <br>
					• AP Biology <br>
					• AP Chemistry <br>
					• AP Physics 1 <br>
					• AP Physics C: Mechanics
				</p>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<h4 class="title fc-secondary tt-uppercase">
					ELECTIVES
				</h4>

				<p class="maindesc">
					• Research & Debate <br>
					• Creative Coding <br>
					• Visual/Performing Arts <br>
					• Economics <br>
					• Psychology <br>
					• Health Education <br>
					• Business & Entrepreneurship <br>
					• Computer Science <br>
					• AP Computer Science A <br>
					• AP Music Theory <br>
					• AP Art & Design <br>
					• AP Psychology <br>
					• AP Microeconomics <br>
					• AP Macroeconomics
				</p>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<h4 class="title fc-secondary tt-uppercase">
					WORLD LANGUAGES
				</h4>

				<p class="maindesc">
					• Spanish I-IV <br>
					• Hispanic Literature & Film <br>
					• French I-IV <br>
					• Francophone Literature <br>
					• Mandarin I-IV <br>
					• German I-III <br>
					• Latin I-III <br>
					• AP Spanish Lang & Culture <br>
					• AP French Lang & Culture <br>
					• AP Chinese Lang & Culture
				</p>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<h4 class="title fc-secondary tt-uppercase">
					social studies
				</h4>

				<p class="maindesc">
					• History of the UAE <br>
					• Ancient World History <br>
					• Modern World History <br>
					• U.S. History <br>
					• U.S. Government & Politics <br>
					• European History <br>
					• AP U.S. History <br>
					• AP European History <br>
					• AP World History: Modern
				</p>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<h4 class="title fc-secondary tt-uppercase">
					english
				</h4>

				<p class="maindesc">
					• English 7 <br>
					• English 8 <br>
					• English 9 <br>
					• English 10 <br>
					• English 11 <br>
					• English 12 <br>
					• AP English Language & Lit <br>
					• AP English Lit & Comp
				</p>
			</div>
		</div>
	</div>
</section>

<!-- Curriculum -->
<section class="sec-padding">
	<div class="container">
		<h2 class="maintitle fc-secondary mbpx-10">
			IB Curriculum
		</h2>

		<p class="maindesc fc-primary">
			Dwight is the first school in the world authorized to offer an online IB Diploma Programme. The programme is an option for students in grades 11 and 12. Successful graduates have the opportunity to earn the prestigious International Baccalaureate diploma. Accreditation by World Academy of Sport gives athletes flexibility to pursue the IB Diploma over three years. Classes include:
		</p>

		<div class="row mtpx-40">
			<div class="col-md-6">
				<p class="maindesc fc-primary">
					• IB DP Sports, Exercise and Health Science <br>
					• IB DP Biology <br>
					• IB DP Physics <br>
					• IB DP Chemistry <br>
					• IB DP Business Management <br>
					• IB DP History <br>
					• IB DP Economics
				</p>
			</div>

			<div class="col-md-6">
				<p class="maindesc fc-primary">
					• IB DP Spanish <br>
					• IB DP Mandarin <br>
					• IB DP English A Literature <br>
					• IB DP Math Analysis and Approaches <br>
					• IB DP Math Application and Interpretation <br>
					• Theory of Knowledge, Extended Essay, CAS
				</p>
			</div>
		</div>
	</div>
</section>


<?php include ($path.'/inc/footer.php'); ?>