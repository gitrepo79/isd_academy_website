<footer class="bg-secondary ptpx-20 pbpx-20">
	<div class="container">
		<ul class="unstyled inline footer-pagination">
			<li>
				<a href="https://goo.gl/maps/BF3ZoEhZhjau3o8b6" target="_blank">Location</a>
			</li>

			<li>
				<a href="https://isddubai.com/about">About</a>
			</li>

			<li>
				<a href="https://isddubai.com/privacy-policy">Privacy Policy</a>
			</li>

			<li>
				<a href="https://isddubai.com/terms-conditions">Terms &amp; Conditions</a>
			</li>
		</ul>
	</div>
</footer>

<footer class="bg-primary ptpx-10 pbpx-10">
	<div class="container">
		<p class="text-center fs-small fc-white">
			Copyright © 2022. All Rights Reserved.
		</p>
	</div>
</footer>

</main>

<script src="/assets-web/js/functions.js"></script>
<script src="/assets-web/js/script.js"></script>

<script>
	$('.partner-slider').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 4,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

</script>

</body>
</html>