<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" type="image/x-icon" href="/assets-web/images/favicon.png" />
	<title>Academy ISD</title>

	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="/assets-web/css/style.css">
</head>
<body>
	
<main class="app-container">

	<header class="primary-header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-3">
					<a href="/" class="logo">
						<img src="/assets-web/images/academy-logo.png" alt="">
					</a>
				</div>

				<div class="col-9">
					<ul class="unstyled inline header-navigation">
						<li>
							<a href="/">
								Home
							</a>
						</li>

						<li>
							<a href="#">
								ISD Academy
							</a>

							<ul class="unstyled dropdown">
								<li>
									<a href="/facilities">
										Facilities
									</a>
								</li>

								<!-- <li>
									<a href="#">
										Sports Programs
									</a>
								</li> -->

								<li>
									<a href="/education">
										Education
									</a>
								</li>

								<li>
									<a href="/accommodation">
										Accommodation
									</a>
								</li>

								<li>
									<a href="/nutrition">
										Nutrition
									</a>
								</li>
							</ul>
						</li>

						<!-- <li>
							<a href="#">
								FAQs
							</a>
						</li> -->

						<li>
							<a href="/contactus">
								Contact
							</a>
						</li>
						
						<li>
							<a href="https://isddubai.com" class="btn --btn-secondary fc-white" target="_blank">
								ISD Dubai
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>