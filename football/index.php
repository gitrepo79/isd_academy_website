<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Football Page";
    $keywords = "";
    $desc = "";
    $pageclass = "footballpg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner" style="background-color: #01b1ae">
	<img src="/assets-web/images/football-banner.jpg" alt="" class="m-auto">
</section>

<!-- Football Overview -->
<section class="sec-padding" style="padding-bottom: 0">
	<div class="container">
		<h2 class="maintitle fc-primary tt-uppercase mbpx-30">
			Football Overview
		</h2>

		<p class="maindesc fc-primary">
			ISD Academy’s football program retains global recognition by developing student-athletes within an atmosphere that cannot be found anywhere else in the region. From a training methodology focused on individual improvement and team strategies to an academic schedule and curriculum that mirrors collegiate programs, student-athletes have access to a wide network of resources that ensures they are supported throughout their career.
			<br /><br />

            Accolades range from college commitments to national and professional league call-ups. ISD’s total athletic development model and unparalleled support system provides student-athletes with an opportunity and a platform to reach their peak potential both academically and athletically.
		</p>
	</div>
	
	<picture class="mtpx-30 d-block">
		<img src="/assets-web/images/football-overview.jpg" alt="">
	</picture>
</section>

<!-- Technology Facilites -->
<section class="bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Technology & Facilities
				</h2>

				<p class="maindesc">
					Six full-size FIFA standard, UAEFA-approved football pitches <br />
                    Only indoor, full-size 3G football pitch in UAE <br /><br />
                    <strong style="color: #f7252b">Five outdoor pitches:</strong> four natural grass (one with stadium-seating for up to 2,500) & one artificial turf <br /><br />
                    
                    <strong style="color: #f7252b">Catapult:</strong> wearable, GPS-tracking technology that measures everything from distances sprinted and ran to speed to impacts on certain body parts <br /><br />
                    
                    <strong style="color: #f7252b">VEO Camera:</strong> high-performance camera to film all matches and provide video analysis of players’ technique and performance <br /><br />
                    
                    <strong style="color: #f7252b">Footlab:</strong> performance and entertainment center to measure speed, dribbling technique, kick power and accuracy, and compete against other players through Challenge, Street Soccer, Footvolley, and a 5-a-side pitch <br /><br />
                    
                    <strong style="color: #f7252b">Eupepsia Performance Lab:</strong> state-of-the-art strength and conditioning facility focused on optimizing athletic performance through baseline testing and periodic measurement followed by sport-specific, personalized training program to attack deficiencies and maximize strengths <br /><br />
                    
                    <strong style="color: #f7252b">Eupepsia Recovery:</strong> whole-body cryotherapy, oxygen & hydrogen therapy, compression remedy, magneto therapy, and infrared sauna <br /><br />
                    
                    <strong style="color: #f7252b">Eupepsia Medical Clinic:</strong> sports physiotherapy, recovery, and conditioning, wellness therapies, diet & nutrition, and ayurvedic medicine
				</p>

			</div>

		</div>
	</div>
</section>


<!-- Program Structure -->
<section class="sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle fc-primary tt-uppercase lh-medium mbpx-30">
					Football Program Structure
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="box --football-program-structure">
					<h5 class="title mbpx-20" style="color: #f7252b">First Term (16 weeks)</h5>

					<p class="fc-primary">
						• Preseason and Measurements <br />
						  (4-6 weeks) <br />
						• General strength & conditioning <br />
						• Fundamentals correcting & teaching <br />
						• Technical & tactical abilities <br />
						• Acceleration & running mechanics training <br />
						• Baseline measurements & assessments <br />
						• Footlab baselines <br />
						• Competitive Season (8-10 weeks) <br />
						• Maintain conditioning level EPL & track) <br />
						• Injury prevention (EPL) <br />
						• Improving technical abilities at specific positions <br />
						• Tactical skills sessions (game understanding & decision making) <br />
						• Competition preparation (tactics) <br />
						• Mental training & preparation <br />
						• Video analysis sessions <br />
						• Specific position training sessions <br />
						• Competition - tournaments, friendlies, and leagues <br /><br />

						BREAK WITH 2 WEEKS OF REST <br />
						Individualized strength and conditioning plans to keep in shape and prevent injury
					</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box --football-program-structure">
					<h5 class="title mbpx-20" style="color: #f7252b">Second term (12 weeks)</h5>

					<p class="fc-primary">
						• Preseason and Measurements <br />
						  (2 weeks) <br />
						• General strength & conditioning <br />
						• Fundamentals correcting & teaching <br />
						• Technical & tactical abilities<br />
						• Acceleration & running mechanics training<br />
						• Baseline measurements & assessments<br />
						• Footlab testing<br />
						• Competitive Season (10 weeks)<br />
						• Maintain conditioning level (EPL & track)<br />
						• Injury prevention (EPL)<br />
						• Improving technical abilities at specific  positions<br />
						• Tactical skills sessions (game  understanding & decision making)<br />
						• Competition preparation (tactics)<br />
						• Mental training & preparation<br />
						• Video analysis sessions<br />
						• Specific position training sessions<br />
						• Competition - tournaments, friendlies, and leagues
					</p>
				</div>
			</div>

			<div class="col-md-4">
				<div class="box --football-program-structure">
					<h5 class="title mbpx-20" style="color: #f7252b">Third term (12 weeks)</h5>

					<p class="fc-primary">
						• General strength & conditioning <br>
						• Fundamentals correcting & teaching <br>
						• Technical & tactical abilities <br>
						• Acceleration & running mechanics  training <br>
						• Measurements & assessments <br>
						• Footlab testing <br>
						• Maintain conditioning level (EPL & track) <br>
						• Injury prevention (EPL) <br>
						• Improving technical abilities at specific positions <br>
						• Tactical skills sessions (game understanding & decision making) <br>
						• Competition preparation (tactics) <br>
						• Mental training & preparation <br>
						• Video analysis sessions <br>
						• Specific position training sessions <br>
						• Competition - tournaments, friendlies, and international competition*
					</p>
				</div>
			</div>
		</div>


	</div>
</section>


<!-- Development Process -->
<section class="bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					development process
				</h2>
				
				<picture class="mtpx-30 d-block">
            		<img src="/assets-web/images/football-development-process.jpg" alt="">
            	</picture>

				<p class="maindesc mtpx-30">
					Over 10 months of training and competition, ISD Academy football provides a platform for athletes to elevate their games to the highest level. Through the combination of LaLiga Academy and exclusive ISD Academy daily trainings with acclaimed coaches from all regions of the world, top-level leagues and tournaments in Dubai and Abu Dhabi, and a yearly trip to Madrid, Spain for the elite players in our academy to showcase their skills in front of European professional organizations, ISD Academy offers student-athletes an unparalleled opportunity to improve their football skills and reach the level that they wish to achieve, whether that is an athletic scholarship to college or university in USA or Europe, or an opportunity with a professional team!
				</p>

			</div>

		</div>
	</div>
</section>

<!-- LaLiga HPC -->
<section class="sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle fc-primary tt-uppercase lh-medium mbpx-30">
					LaLiga Academy HPC
				</h2>
				
				<picture class="mtpx-30 d-block">
            		<img src="/assets-web/images/football-laliga-hpc.jpg" alt="">
            	</picture>

				<p class="maindesc fc-primary mtpx-30">
					LaLiga Academy High Performance Center (HPC) gives our student-athletes a platform to train with the best youth footballers in Dubai through three weekly training sessions and high-level tournaments, leagues, and friendly matches. Using LaLiga methodology, our HPC coaches have a proven track record of developing talent in the region, already sending seven players directly to clubs in UAE professional leagues. 
				</p>

			</div>

		</div>
	</div>
</section>

<!-- Spain Tour -->
<section class="bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Spring Trip to Madrid, Spain
				</h2>
				
				<picture class="mtpx-30 d-block">
            		<img src="/assets-web/images/football-spain-trip.jpg" alt="">
            	</picture>

				<p class="maindesc mtpx-30">
					At the end of the third term, our top players will earn a chance to travel to Spain and play friendlies and tournaments against international clubs in an exciting, competitive setting, while getting scouted by professional teams in the area. Our goal is for some of our players to sign contracts in Europe, and the others to earn scholarships to colleges and universities in the US or Europe.
				</p>

			</div>

		</div>
	</div>
</section>

<!-- LaLiga Coaches -->
<section class="sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle fc-primary tt-uppercase lh-medium mbpx-30">
					Coaching Staff
				</h2>

				<p class="maindesc fc-primary">
					ISD Academy has selected the finest coaches for the academy, holding the highest UEFA pro-certifications and a wealth of experience that has been developed over years of working with LaLiga clubs' youth teams in Spain and internationally across Europe, Asia, the Middle East, and North and South America. 
				</p>

			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-zarco.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Jose Maria Zarco</p>
							<p class="designation fc-primary tt-uppercase">Technical Director</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-ivan.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Ivan Perez Vasquez</p>
							<p class="designation fc-primary tt-uppercase">Head Coach - Dubai</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-hernandez.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Javier Hernandez</p>
							<p class="designation fc-primary tt-uppercase">Head Coach - Abu Dhabi</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-martinez.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Javier Martinez</p>
							<p class="designation fc-primary tt-uppercase">Coach</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-balsera.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Sergio Balsera</p>
							<p class="designation fc-primary tt-uppercase">Coach</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-limones.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Daniel Limones</p>
							<p class="designation fc-primary tt-uppercase">Coach</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-soto.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Daniel Limones</p>
							<p class="designation fc-primary tt-uppercase">Video-Analysis Coach</p>
						</div>
						
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="box --coach-box mtpx-20">
					<picture>
						<img src="/assets-web/images/coach-fernandez.jpg" alt="">
					</picture>

					<div class="detail mtpx-12" style="display: flex; align-items: center;">
					    <div>
							<img src="/assets-web/images/spain-logo.png" alt="">
						</div>
						
						<div class="ml-4">
							<p class="name fw-bold tt-uppercase" style="color: #f7252b;">Xende Fernandez</p>
							<p class="designation fc-primary tt-uppercase">Coach</p>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include ($path.'/inc/footer.php'); ?>