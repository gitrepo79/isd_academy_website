<?php
	// $path = $_SERVER["DOCUMENT_ROOT"] ."/repo";
    $path = $_SERVER["DOCUMENT_ROOT"];
    $title = "Accommodation Page";
    $keywords = "";
    $desc = "";
    $pageclass = "accommodationpg";
?>

<?php include ($path.'/inc/header.php'); ?>

<section class="hero-banner" style="background-color: #01b1ae">
	<img src="/assets-web/images/accomodation-banner.jpg" alt="" class="m-auto">
</section>

<section class="facilities-box sec-padding">
	<div class="container">
		<h2 class="maintitle fc-primary tt-uppercase mbpx-30">
			Emirates Sports <br> Apartments
		</h2>

		<picture class="mbpx-30 d-block">
			<img src="/assets-web/images/accomodation-sports-apartments.jpg" alt="" style="object-fit: cover">
		</picture>

		<p class="maindesc fc-primary">
			Located on-site at Inspiratus Sports District, and within driving distance of all major attractions across Dubai, Emirates Sports Apartments is a luxury, fully-furnished accommodation option for students of ISD Academy. With one and two-bedroom options, fitting groups of 2 and 4 respectively <br><br>

			Offering both long-term apartments and hotel rooms, this is an easy, attractive option for families, guests, teams, or visitors as well.
		</p>
	</div>
</section>


<section class="facilities-box bg-primary sec-padding">
	<div class="container">	
		<div class="row">
			<div class="col-md-12">
				<h2 class="maintitle tt-uppercase lh-medium mbpx-30">
					Canal Residence <br> West Apartments
				</h2>

				<picture class="mbpx-30 d-block">
					<img src="/assets-web/images/accomodation-apartments.jpg" alt="">
				</picture>

				<p class="maindesc">
					Each athlete at ISD Academy will sit down with a sports nutritionist from Theo’s Point in their first week on campus to design a meal plan that meets their specific body and performance goals. With options such as “fitness,” ‘weight management,’ ‘healthy best,’ ‘vegan,’ and ‘managing intolerances,’ there is a meal plan that fits each student-athlete’s nutritional requirements. Plans can also be further personalized to meet specific dietary restrictions or preferences. <br><br>

					Once the meal plan is underway, student-athletes will have a meeting scheduled with our Theo’s Point nutritionists every two weeks to check in on their progression of body and performance aspirations. In this one hour, bi-weekly meeting, the nutritionist will perform a series of tests that measure parameters such as weight, body fat mass and percentage, and total body water, proteins, and minerals. He or she will then analyze the results for our athletes to give them a more complete understanding of what they have been improving upon, and where they will require more focus. This analysis will allow the Theo’s Point team to further customize the athlete’s meal plan to achieve maximum results! 
				</p>
			</div>

		</div>
	</div>
</section>

<?php include ($path.'/inc/footer.php'); ?>